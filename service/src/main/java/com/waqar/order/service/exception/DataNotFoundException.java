package com.waqar.order.service.exception;

public class DataNotFoundException extends BaseException {

    public DataNotFoundException(int errorCode, String message) {
        super( errorCode, message );
    }

}

package com.waqar.order.service.rest.dto.v1;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDTO {

    private int errorCode;
    private String moreInfo;
    private String userMessage;
    private String developerMessage;

}

package com.waqar.order.service.service;

import com.waqar.order.service.constants.Error;
import com.waqar.order.service.constants.ErrorMessages;
import com.waqar.order.service.exception.BaseException;
import com.waqar.order.service.exception.DataNotFoundException;
import com.waqar.order.service.rest.dto.v1.OrderDTO;
import com.waqar.order.service.service.impl.OrderService;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Override
    public OrderDTO getOrder(int id ) throws BaseException {
        if( id == 2) {
            throw new DataNotFoundException( Error.DATA_NOT_FOUND.getCode(), ErrorMessages.NO_ORDER_FOUND.toString() );
        }
        return OrderDTO.builder().id(1L).description("Draft Order Found").build();
    }

}

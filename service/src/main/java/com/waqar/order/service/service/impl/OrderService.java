package com.waqar.order.service.service.impl;

import com.waqar.order.service.exception.BaseException;
import com.waqar.order.service.rest.dto.v1.OrderDTO;

public interface OrderService {

    OrderDTO getOrder(int id ) throws BaseException;

}

package com.waqar.order.service.rest.controller.v1;

import com.waqar.order.service.exception.BaseException;
import com.waqar.order.service.rest.controller.OrderServiceBaseResource;
import com.waqar.order.service.rest.dto.v1.OrderDTO;
import com.waqar.order.service.service.impl.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping( "/v1" )
public class OrderResource extends OrderServiceBaseResource {

    @Autowired
    OrderService orderService;

    @GetMapping(value = "/{orderId}")
    public ResponseEntity<?> getDraftOrder( @PathVariable int orderId  ) throws BaseException {
        OrderDTO orderDTO = orderService.getOrder( orderId );
        return ResponseEntity.ok().body(orderDTO);
    }
}

package com.waqar.order.service.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BaseException extends Exception {

    protected int errorCode;
    protected String message;

}

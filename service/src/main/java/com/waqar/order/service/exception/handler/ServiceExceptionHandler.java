package com.waqar.order.service.exception.handler;

import com.waqar.order.service.constants.Error;
import com.waqar.order.service.constants.ErrorMessages;
import com.waqar.order.service.exception.DataNotFoundException;
import com.waqar.order.service.rest.dto.v1.ErrorDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ServiceExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDTO> exceptionHandler(Exception exception ) {
        ErrorDTO errorDTO ;
        ResponseEntity<ErrorDTO> responseEntity ;
        if( exception instanceof DataNotFoundException) {
            DataNotFoundException dataNotFoundException = (DataNotFoundException) exception;
            errorDTO = new ErrorDTO( dataNotFoundException.getErrorCode(),
                    ErrorMessages.NO_DATA_AVAILABLE.toString(),
                    Error.DATA_NOT_FOUND.getUserMessage(), exception.getMessage() );
            responseEntity = new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST);
        }
        else {
            errorDTO = new ErrorDTO( Error.INTERNAL_SERVER_ERROR.getCode(),
                    ErrorMessages.NO_RESPONSE.toString(),
                    Error.INTERNAL_SERVER_ERROR.getUserMessage(), exception.getMessage() );
            responseEntity = new ResponseEntity<>(errorDTO, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }
}

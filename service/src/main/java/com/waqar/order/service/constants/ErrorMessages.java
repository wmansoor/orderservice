package com.waqar.order.service.constants;

public enum ErrorMessages {

    NO_ORDER_FOUND("No draft order found."),
    NO_DATA_AVAILABLE("No data Available."),
    NO_RESPONSE("No response available for current request.");

    String message;

    ErrorMessages(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return this.message;
    }
}

package com.waqar.order.service.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderServiceBaseResource {

    public static final String OK = "OK";

    @GetMapping("/ping" )
    public ResponseEntity<?> ping() {
        return ResponseEntity.ok().body(OK);
    }

}

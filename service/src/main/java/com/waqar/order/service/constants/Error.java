package com.waqar.order.service.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Error {

    DATA_NOT_FOUND( 400,"No draft order available." ),
    INTERNAL_SERVER_ERROR( 500,"Unable to Process current request.");

    private int code;
    private String userMessage;

}

# GRADLE BASED JAVA PROJECT STRUCTURE
SpringBoot have been used to configure the project structure

### Gradle Structure contains following
 - Gradle Wrapper Properties have been used
 - Sub-projects structuring in gradle
 
### Project Structure contains following
 - Service Layer
 - Rest Controller
 - Exceptions structure
 - Models and DTOs
 